'use strict';

var _ = require('underscore'),
  Device = require('../models/devices').Device,
  Client = require('node-rest-client').Client;


function Command(path) {
  /*
  * http://rpiguillem:8083/ZWaveAPI/Run/devices[7].instances[0].commandClasses[37].Set(0)
  * curl -H "Content-Type: application/json" -d '{"command":"light-on"}' http://localhost:8080/devices/53c628ed9c4040392c2ce756/commands
  */
  this.path = path || 'http://192.168.1.15:8083/ZWaveAPI/Run/';
  this.client = new Client();

  this.commands = {
    'on': 255,
    'off': 0
  };
}

Command.prototype.post = function(id, command, next) {
  var that = this,
    args;

  Device.findById(id, function (err, device) {
    if (err) {
      next(err);
    } else if (device) {
      args = {
        path: {
          id: device.zwid,
          command: that.commands[command]
        }
      };

      console.log("COMMAND RECEIVED");

      that.client.post(
        that.path + 'devices%5B${id}%5D.instances%5B0%5D.commandClasses%5B37%5D.Set%28${command}%29',
        args,
        next
      ).on('error', next);
    } else {
      next();
    }
  });
};

module.exports.Command = Command;
