'use strict';

var Client = require('node-rest-client').Client;


function Device(path) {
  this.path = path || 'http://localhost:8888';
  this.path = this.path + '/devices';
  this.client = new Client();
}

Device.prototype.get = function (id, next) {
  var that = this;

  this.client.get(
    that.path + '${id}/',
    {path: {id: id}},
    next
  );
};

Device.prototype.update = function (id, device, next) {
  var that = this,
    args = {
      data: device,
      headers: {'Content-Type': 'application/json'},
      path: {id: id},
    };

  this.client.post(
    that.path + '/${id}/',
    args,
    function (data, res) {
      if (res.statusCode == 500) {
        next(data, null);
      } else {
        next(null, JSON.parse(data));
      }
    }
  ).on('error', function (err) {
    next(err, null);
  });
};

Device.prototype.post = function(device, next) {
  var that = this,
    args = {
      data: device,
      headers: {'Content-Type': 'application/json'}
    };

  this.client.post(
    that.path,
    args,
    function (data, res) {
      if (res.statusCode == 500) {
        next(data, null);
      } else {
        next(null, JSON.parse(data));
      }
    }
  ).on('error', function (err) {
    next(err, null);
  });
};

module.exports.Device = Device;
