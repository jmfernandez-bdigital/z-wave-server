'use strict';

var api = require('../controllers/api'),
  commands = require('../controllers/commands');

module.exports = function (app) {
  app.post('/smartHomeGateway/rest/v2/data', api.post);
  app.post('/devices/:id/commands', commands.post);
};
