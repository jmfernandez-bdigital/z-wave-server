
/**
 * Module dependencies.
 */

var express = require('express'),
  http = require('http'),
  path = require('path'),
  zlib = require('zlib'),
  gzip = require('./utils/gzip_utils'),
  mongoose = require('mongoose');

var common = require('./config/common'),
  envConfig = common.config(),
  CFG_SERVER = envConfig.server,
  CFG_STORE_MONGO = envConfig.storeMongo,
  port = process.env.PORT || CFG_SERVER.port,
  forks = process.env.FORKS || CFG_SERVER.forks;

var app = express(),
  server = http.createServer(app);

// all environments
app.set('port', port);

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(gzip());
app.use(express.urlencoded());
app.use(express.multipart());
app.use(express.methodOverride());
app.use(app.router);

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

//Start mongodb connection
app.mongoClient = mongoose.connect(
  CFG_STORE_MONGO.host,
  CFG_STORE_MONGO.dbname
);

// Bootstrap routes
require('./config/routes')(app);

server.listen(app.get('port'), function () {
  console.log("Server listening on port " + app.get('port') + ' in "' + app.settings.env + '" mode');
});


// Add other stuff to app
app.envConfig = envConfig;
module.exports = app;
