Cloud4All---Context-Aware-Server
================================

Description
-----------

The Z-Wave Server (CAS) developed in NodeJs provides a platform that process z-way data from Raspberry PI and sends it to the context aware server.

License
-------

This project has been developed by Barcelona Digital Health department. Device reporter is shared under New BSD license. This project folder includes a license file. You can share and distribute this project and built software using New BSD license. Please, send any feedback to http://www.cloud4all.info

Quick Start & Examples
----------------------

The best way to get started with the server is running it using node. You need also to have the context-aware-server running in localhost.

To switch on an actuator:

```bash
curl -X POST -H "Content-Type: application/json" --data '{"command":"on"}' http://localhost:8080/devices/:id/commands
```
Or to switch off an actuator:

```bash
curl -X POST -H "Content-Type: application/json" --data '{"command":"off"}' http://localhost:8080/devices/:id/commands


Help to use Rapsberry Pi - zwave:
----------------------------------

Connect to: http://192.168.1.19:8083/
