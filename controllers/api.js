/**
 * Sensor API routes.
**/

'use strict';

var app = require('../app'),
  async = require('async'),
  _ = require('underscore'),
  DevicesAPI = require('../api/devices-api'),
  DataAPI = require('../api/data-api'),
  Device = require('../models/devices').Device;


exports.post = function (req, res) {
  /**
   * Get data from hub system z-wave
   * 1. searches of existent device
   * 2. if existent device, retrieve sensors uuid and send data to cas
   * 3. if inexistent device, create device and sensors, when created, send data data to cas
  **/

  var id = req.body.id,
    dataAPI = new DataAPI.Data(),
    devicesAPI = new DevicesAPI.Device(),
    devices = req.body.devices;

  var sendData = function (device, input, cb) {
    device.sensors.map(function (sensor) {
      try {
        sensor.measurements = _.find(input.sensors, function (el) {
          return el.path === sensor.devid;
        }).measurements || {};
      } catch (err) {
        sensor.measurements = {};
      }
    });
    async.each(device.sensors, function(sensor, callback) {
      dataAPI.post(sensor.id, sensor.measurements, callback);
    }, cb);
  };

  var newDevice = function (el, cb) {
    devicesAPI.post(el, function (err, device) {
      if (err) {
        return cb(err, null);
      }
      newDevice = new Device(device);
      newDevice.save(function (err, item) {
        return cb(err, item);
      });
    });
  };

  var callback = function (el, cb) {
    Device.findOne({serial: el.serial}, function (err, device) {
      if (err) {
        cb(err);
      } else if (device) {
        // update the new sensors in CAS
        var devids = device.sensors.map(function (e) {return e.devid});
        var newsensors = false;
        el.sensors.forEach(function (e) {
          if (!_.contains(devids, e.devid)) {
            newsensors = true;
            device.sensors.push(e);
          }
        });

        if (newsensors) {
          devicesAPI.update(device.id, device, function (err, updatedDevice) {
            // update the sensors in our database
            Device.findOneAndUpdate(
              {serial: el.serial},
              {sensors: updatedDevice.sensors},
              {upsert: true},
              function (err, dev) {
                // send data
                sendData(dev, el, cb);
              });
          });
        } else {
          sendData(device, el, cb);
        }
      } else {
        newDevice(el, function (err, device) {
          if (err) {
            cb(err);
          } else {
            sendData(device, el, cb);
          }
        });
      }
    });
  };

  devices = devices.map(function (device) {
    // default position (0,0):
    device.gps = device.gps || [0, 0];
    device.location = device.location || "default";
    device.serial = id + ':' + device.path;

    device.sensors = device.sensors.map(function (sensor) {
      sensor.devid = sensor.path;
      sensor.type = sensor.datatype;
      return sensor;
    });
    return device;
  });

  async.each(devices, callback, function (err) {
    if (err) {
      res.send(500, err);
    } else {
      res.send({error_code: 0});
    }
  });
};
